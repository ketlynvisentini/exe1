package calculoseminterface;

import java.util.Scanner;

public class Nota {

    public int quantAvaliacao;
    public float peso=0;
    public float somaPeso=0;
    public float nota;
    public float media;
    public float[] medias = new float[3];
    private int indice = 0;

    public void calcularTrimestre() {
        Scanner input = new Scanner(System.in);
        System.out.println("informe a quantidade de avaliacoes : ");
        quantAvaliacao = input.nextInt();
        for (int i = 0; i < quantAvaliacao; i++) {
            System.out.println("o peso desta avaliacao : ");
            peso = input.nextFloat();
            somaPeso = peso + somaPeso;
            System.out.println("a nota tirada nesta avaliacao: ");
            nota = input.nextFloat();
            if (nota == 0) {
                nota = (float) 0.01;
            }
            media = ((nota / 10) * peso) + media;
        }
        if (somaPeso == 10) {
            medias[indice] = media;
            indice++;
        } else {
            System.out.println("valor de peso invalido: ");
        }
    }
}
